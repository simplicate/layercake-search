<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class SearchController extends LayerCakeAppController {

	var $name       = 'Search';
    var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'LayerCake.MenuTree', 'LayerCake.ThisUrl' );
    var $uses       = array( 'Search.SearchIndex' );

	function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'index' );
	}

    function index() {

        // redirect ?query=params to /named:params
        if( isset( $this->data['Search']['q'] ) ) {
            $q = $this->data['Search']['q'];
            $this->redirect( "/" . $this->params['url']['url'] . "/q:$q" );
            exit;
        }

        // 100 most common english words
        $commonWords = Configure::read( 'SearchEngine.commonWords');

        // do we have a query?
        if( isset( $this->params['named']['q'] ) ) {

            // get query string
            $q      = $this->params['named']['q'];
            $q      = strtolower( $q );
            $q      = preg_replace( '/[\W\s]+/', ' ', $q );
            $q      = trim( $q );

            // starting fields
            $fields = array(
                '*', '1 as score'
            );

            // starting conditions
            $conditions = array( "OR" => array() );

            // add each word individually
            $words = explode( ' ', $q );
            $order = "score DESC";
            foreach( $words AS $counter => $word ) {

                // punish common and short individual words
                $multiplier = 1;
                if( in_array( $word, $commonWords ) ) {
                    $multiplier = .1;
                    if( strlen( $word ) <= 3 ) {
                        $multiplier = .05;
                    }
                }

                $fields[]   = "((LENGTH(data) - LENGTH(REPLACE(data, '$word', ''))) / LENGTH('$word')) * $multiplier AS cnt" . $counter;
                $order      = 'cnt' . $counter . ' + ' . $order;

                $conditions["OR"][] = "data LIKE '%$word%'";
            }

            if( count( $words ) > 1 ) {
                $fields[] = "((LENGTH(data) - LENGTH(REPLACE(data, '$q', ''))) / LENGTH('$q') * 2 ) AS cntx";
                $order = 'cntx' . ' + ' . $order;
            }

            $this->paginate['fields']       = $fields;
            $this->paginate['conditions']   = $conditions;
            $this->paginate['order']        = array( $order );

            $this->set( 'results',      $this->paginate('SearchIndex') );
            $this->set( 'search_term',  $this->params['named']['q'] );

            $this->render( 'results' );
        } else {
            $this->render( 'index' );
        }
    }
}