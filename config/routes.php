<?
    $search_url = Configure::read('SearchEngine.url');
    Router::connect(
        "$search_url/*",
        array(
                'plugin'     => 'search',
                'controller' => 'search',
                'action'     => 'index',
        )
    );