<?= $this->Form->create( 'Search', array( 'url' => Configure::read('SearchEngine.url') ) ); ?>
	<?= $this->Form->input( 'q', array( 'label' => 'Search', 'placeholder' => 'Search Term' ) ); ?>
    <button class="button" type="submit">Search</button>
</form>