<?php
    $this->set( 'title_for_layout', Configure::read('SearchEngine.title') );
    $this->params['url']['url'] = Configure::read('SearchEngine.url');
?>

<? if( count( $results ) ): ?>
    <? foreach ( $results as $result ): ?>
        <p>
            <a href="<?= $result['SearchIndex']['link']; ?>"><?= $result['SearchIndex']['name']; ?></a><br>
            <?= $this->SearchHelper->snippet( $result['SearchIndex']['summary'], $search_term ); ?>
        </p>
    <? endforeach; ?>
<? else: ?>
    <p>No matching results found</p>
<? endif; ?>

<? if( $paginator->hasPage(2) ): ?>
    <p class="pagination">
        <?= $paginator->prev('«'); ?>
        <?= $paginator->numbers(); ?>
        <?= $paginator->next('»'); ?>
    </p>
<? endif; ?>