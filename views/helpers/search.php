<?php
class SearchHelper extends AppHelper {

    function snippet( $text, $keyword ) {
        $text    = strip_tags( $text );
        $size    = 110;
        $start   = stripos( $text, $keyword ) - $size;

        if( $start < 0 ) {
            $size += ( abs( $start ) );
            $start = 0;
        }

        $end     = stripos($text, $keyword) + strlen($keyword) + $size;
        $length  = $end - $start;

        $snippet = mb_substr( $text, $start, $length );
        $snippet = preg_replace("/$keyword/i", "<strong>\$0</strong>", $snippet);

        if( $start != 0 ) {
            $snippet = preg_replace( "/^[^\s]+\s/", "", $snippet );
            $snippet = '...' . $snippet;
        }

        $snippet = preg_replace( "/\s[^\s]+$/", "", $snippet );
        $snippet = trim( $snippet ) . '...';

        return $snippet;
    }

}