<?php
/**
 * Rebuild Search Index
 */
class RebuildIndexShell extends Shell {

    // main function call
    function main() {

        App::import('Model', 'Search.SearchIndex');
        $si = new SearchIndex;

        // pages
        if( $this->command == 'pages' ) {
            $si->deleteAll( array( 'model' => 'Page' ) ) ;

            App::import( 'Model', 'Content.Page' );
            $model = new Page;
            foreach( $model->find( 'all', array( 'contain' => array() ) ) AS $rec ) {
                $model->save( $rec );
            }
        }

        // blog articles
        if( $this->command == 'blogs' ) {
            $si->deleteAll( array( 'model' => 'Blog' ) ) ;

            App::import( 'Model', 'Blog.Blog' );
            $model = new Blog;
            foreach( $model->find( 'all', array( 'contain' => array() ) ) AS $rec ) {
                $model->save( $rec );
            }
        }
    }
}