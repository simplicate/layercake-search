<?php
class SearchableBehavior extends ModelBehavior {

    var $settings   = array();
	var $model      = null;

	var $_defaults  = array(
        'ignoreColumns'     => array(),
        'summaryColumns'    => array(),
	);

	var $SearchIndex = null;


	function setup( &$model, $settings = array() ) {
		$settings = array_merge($this->_defaults, $settings);
		$this->settings[$model->name] = $settings;
		$this->model = &$model;
	}


	function afterSave() {

        // build search fields
        $newInsert    = ( $this->model->id ) ? false : true;
        $foreignKey   = ( $this->model->id ) ? $this->model->id : $this->model->getLastInsertID();

        // load data
        $data       = $this->model->findById( $foreignKey );

        // create all indexes
        $index      = $this->_index( $data );
        $summary    = $this->_summary( $data );
        $name       = $this->_name( $data );
        $link       = $this->_link( $data );
        $language   = $this->_language( $data );
        $misc_1     = $this->_misc_1( $data );
        $misc_2     = $this->_misc_2( $data );
        $misc_3     = $this->_misc_3( $data );
        $misc_4     = $this->_misc_4( $data );
        $misc_5     = $this->_misc_5( $data );

        // load model
        if( !$this->SearchIndex ) {
            $this->SearchIndex = ClassRegistry::init('Search.SearchIndex');
        }

        // create the array
        $searchIndex = array(
            'SearchIndex' => array(
                'model'             => $this->model->name,
                'association_key'   => $foreignKey,
                'data'              => $index,
                'summary'           => $summary,
                'name'              => $name,
                'link'              => $link,
                'language'          => $language,
                'misc_1'            => $misc_1,
                'misc_2'            => $misc_2,
                'misc_3'            => $misc_3,
                'misc_4'            => $misc_4,
                'misc_5'            => $misc_5,
            )
        );

        // is this an update to an existing record?
        if( $newInsert == false ) {

            // find out the id of the existing record
            $searchEntry = $this->SearchIndex->find( 'first', array( 'fields' => array('id'), 'conditions' => array( 'model' => $this->model->name, 'association_key' => $foreignKey ) ) );
            $searchIndex['SearchIndex']['id'] = $searchEntry['SearchIndex']['id'];
        }

        // if we *should* be saving this record
        if( $this->_trigger( $data ) ) {

            // save index
            $this->SearchIndex->create();
            if( $this->SearchIndex->save( $searchIndex ) );

        // don't save this record
        } else {

            // and if it already exists, delete it
            if( $newInsert == false && isset( $searchIndex['SearchIndex']['id'] ) && !empty( $searchIndex['SearchIndex']['id'] ) ) {
                $this->SearchIndex->delete( $searchIndex['SearchIndex']['id'] );
            }
        }

		// return true
        return true;
	}


	// determine whether or not to index this data at all
    function _trigger( $data ) {

        // default is to save
        $trigger = true;

        // does the model have a hook to create the index
        if( method_exists($this->model, 'searchable_trigger') ) {
            $trigger = $this->model->searchable_trigger( $data );
        }

		return $trigger;
	}


    // function to index all the data of the record
	function _index( $data ) {

        // start with nothing
        $index = '';

        // does the model have a hook to create the index
        if( method_exists($this->model, 'searchable_index') ) {

            $index = $this->model->searchable_index( $data );

        // if not, attempt to index data on our own
        } else {

            $indexAr = array();
            $data    = $data[$this->model->name];
            $columns = $this->model->getColumnTypes();
            foreach( $data as $key => $value ) {
                if( is_string( $value ) ) {
                    if( $key != $this->model->primaryKey && isset( $columns[$key] ) && in_array( $columns[$key], array( 'text', 'varchar', 'char', 'string' ) ) && !in_array( $key, $this->settings[$this->model->name]['ignoreColumns'] ) ) {
                        $indexAr[] = $value;
                    }
                }
            }

            $index = join( ' ', $indexAr );
        }

        // clean up index contents
        $index = strip_tags( $index );
        $index = html_entity_decode($index, ENT_NOQUOTES, 'UTF-8');
		$index = preg_replace( '/[\ ]+/', ' ', $index );
        $index = preg_replace( '/[\/\-_]+/', ' ', $index );
        $index = strtolower( $index );
        $index = trim( $index );

		return $index;
	}


	// create just a summary of human readable data for the record
    function _summary( $data ) {

        // start with nothing
        $summary = '';

        // does the model have a hook to create the summary
        if( method_exists($this->model, 'searchable_summary') ) {

            $summary = $this->model->searchable_summary( $data );

        // if not, attempt to index data on our own
        } else {

            $summAr  = array();
            $data    = $data[$this->model->name];
            $columns = $this->model->getColumnTypes();

            foreach( $data as $key => $value ) {
                if( is_string( $value ) ) {
                    if( $key != $this->model->primaryKey && isset( $columns[$key] ) && in_array( $columns[$key], array( 'text', 'varchar', 'char', 'string' ) ) && in_array( $key, $this->settings[$this->model->name]['summaryColumns'] ) ) {
                        $summAr[] = $value;
                    }
                }
            }

            $summary = join( ' ', $summAr );
        }

		// clean up contents
        $summary = strip_tags( $summary );
        $summary = html_entity_decode($summary, ENT_NOQUOTES, 'UTF-8');
		$summary = preg_replace( '/[\ ]+/', ' ', $summary );
        $summary = trim( $summary );

		return $summary;
	}


    // create the human readable name for the record
	function _name( $data ) {

        // start with nothing
        $name = '';

        // does the model have a hook to create the name
        if( method_exists($this->model, 'searchable_name') ) {
            $name = $this->model->searchable_name( $data );

        // if not, attempt to index data on our own
        } else {

            // try different fields
            $data    = $data[$this->model->name];
            foreach( array( 'name', 'title' ) AS $field ) {
                if( isset( $data[$field] ) && !empty( $data[$field] ) ) {
                    $name = $data[$field];
                    break;
                }
            }
        }

		// clean up contents
        $name = strip_tags( $name );
        $name = html_entity_decode($name, ENT_NOQUOTES, 'UTF-8');
		$name = preg_replace( '/[\ ]+/', ' ', $name );
        $name = trim( $name );

        return $name;
	}


	// create the canonical link for the record
    function _link( $data ) {

        // start with nothing
        $link = '';

        // does the model have a hook to create the link
        if( method_exists($this->model, 'searchable_link') ) {
            $link = $this->model->searchable_link( $data );

        // if not, attempt to index data on our own
        } else {

            // try different fields
            $data    = $data[$this->model->name];
            foreach( array( 'slug', 'link', 'url' ) AS $field ) {
                if( isset( $data[$field] ) && !empty( $data[$field] ) ) {
                    $link = $data[$field];
                    break;
                }
            }
        }

        // clean up contents
        $link = trim( $link );
        $link = strtolower( $link );

        return $link;
	}

	// language
    function _language( $data ) {
        foreach( array('lang', 'language') AS $lang_field ) {
            if ( isset( $data[$this->model->name][$lang_field] ) ) {
                return $data[$this->model->name][$lang_field];
            }
        }
	}


	// misc_1 data
    function _misc_1( $data ) {
        return ( method_exists($this->model, 'searchable_misc_1') ) ? $this->model->searchable_misc_1( $data ) : '';
	}

	// misc_2 data
    function _misc_2( $data ) {
        return ( method_exists($this->model, 'searchable_misc_2') ) ? $this->model->searchable_misc_2( $data ) : '';
	}

	// misc_3 data
    function _misc_3( $data ) {
        return ( method_exists($this->model, 'searchable_misc_3') ) ? $this->model->searchable_misc_3( $data ) : '';
	}

	// misc_4 data
    function _misc_4( $data ) {
        return ( method_exists($this->model, 'searchable_misc_4') ) ? $this->model->searchable_misc_4( $data ) : '';
	}

	// misc_5 data
    function _misc_5( $data ) {
        return ( method_exists($this->model, 'searchable_misc_5') ) ? $this->model->searchable_misc_5( $data ) : '';
	}


    function afterDelete( &$model ) {

        if( !$this->SearchIndex ) {
			$this->SearchIndex = ClassRegistry::init('Search.SearchIndex');
		}

		$conditions = array( 'model' => $model->alias, 'association_key' => $model->id );
		$this->SearchIndex->deleteAll( $conditions );
	}


	function search( &$model, $q, $findOptions = array() ) {

        if (!$this->SearchIndex) {
			$this->SearchIndex = ClassRegistry::init('Search.SearchIndex');
		}

		$this->SearchIndex->searchModels($model->name);
		if (!isset($findOptions['conditions'])) {
			$findOptions['conditions'] = array();
		}

		App::import('Core', 'Sanitize');
		$q = Sanitize::escape($q);
		$findOptions['conditions'] = array_merge(
			$findOptions['conditions'], array("MATCH(SearchIndex.data) AGAINST('$q' IN BOOLEAN MODE)")
		);

		return $this->SearchIndex->find('all', $findOptions);
	}
}